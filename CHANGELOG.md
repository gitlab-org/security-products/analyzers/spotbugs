# spotbugs analyzer changelog

## v6.2.1
- Update `report` package from `v5.5.0` to `v5.7.0` (!250)
- Downgrade security report schema from `15.2.1` to `15.1.4` to fix regression. See https://gitlab.com/gitlab-org/gitlab/-/issues/497405#note_2348142562 (!250)

## v6.2.0
- upgrade [`Spotbugs`](https://github.com/spotbugs/spotbugs) version [`4.8.6` => [`4.9.1`](https://github.com/spotbugs/spotbugs/releases/tag/4.9.1)] (!248)
- upgrade `github.com/bmatcuk/doublestar/v4` version [`v4.7.1` => [`v4.8.1`](https://github.com/bmatcuk/doublestar/releases/tag/v4.8.1)] (!248)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.5.0` => [`v5.6.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.6.0)] (!248)

## v6.1.0
- Add logging to clarify pre-compilation support (!245)

## v6.0.0
- Add support for Java 21 and remove support for Java 11 (!243)
- Set Java 21 as system-wide default version (!243)
- Update fixtures for compatibility with jdk21 (!243)
- Upgrade jdk17 from 17.52.19 to 17.54.21 (!243)
- Upgrade ant from 1.10.12 1.10.15 (!243)
- Upgrade gradle from 7.5 to 8.12 (!243)
- Upgrade grails from 5.2.0 to 5.3.6 (!243)
- Upgrade maven from 3.8.8 to 3.9.9 (!243)
- Upgrade sbt from 1.7.1 to 1.10.7 (!243)
- Upgrade scala from 2.13.8 to 2.13.15 (!243)

## v5.15.0
- upgrade `github.com/gosimple/slug` version [`v1.14.0` => [`v1.15.0`](https://github.com/gosimple/slug/releases/tag/v1.15.0)] (!242)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.3.0` => [`v5.5.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.5.0)] (!242)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.1` => [`v3.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.3.1)] (!242)

## v5.14.0
- Stream log output from spotbugs command using info level (!235)

## v5.13.0
- upgrade `github.com/bmatcuk/doublestar/v4` version [`v4.6.1` => [`v4.7.1`](https://github.com/bmatcuk/doublestar/releases/tag/v4.7.1)] (!238)
- upgrade `github.com/stretchr/testify` version [`v1.9.0` => [`v1.10.0`](https://github.com/stretchr/testify/releases/tag/v1.10.0)] (!238)
- upgrade `github.com/urfave/cli/v2` version [`v2.27.4` => [`v2.27.5`](https://github.com/urfave/cli/releases/tag/v2.27.5)] (!238)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.3.0` => [`v3.4.0`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.4.0)] (!238)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.2.0` => [`v3.2.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.1)] (!238)

## v5.12.0
- Create `gitlab` user and update file/directory permissions in Dockerfile (!224)

## v5.11.0
- Upgrade `maven` from `3.8.6` to `3.8.8` (!231)

## v5.10.0
- Add support for disabling predefined rules via custom rulesets (!230)

## v5.9.0
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.1` => [`v3.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.2.0)] (!229)

## v5.8.1
- Bugfix: Resolve bug preventing use of Java 11 (!226)

## v5.8.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.3` => [`v2.27.4`](https://github.com/urfave/cli/releases/tag/v2.27.4)] (!209)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v5` version [`v5.2.1` => [`v5.3.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v5.3.0)] (!209)

## v5.7.0
- Control how projects are built using `SAST_SPOTBUGS_EXCLUDED_BUILD_PATHS` (!222)

## v5.6.0
- Remove unused `glibc`, `zlibc`, and `gcc` packages (!220)

## v5.5.1
- Bugfix: Resolve build failures for projects that require gcc-libs (!218)

## v5.5.0
- Update `asdf` from `v0.8.1` to `v0.10.2` and use `ASDF_DEFAULT_TOOL_VERSIONS_FILENAME` (!213)

## v5.4.1
- Bugfix: Resolve error on Mac OS X with sbt >= 1.4.0 (!214)

## v5.4.0
- Switch Java17 from `adoptopenjdk-17.0.4+8` to `zulu-musl-17.52.19` (!211)
- Switch Java11 from `adoptopenjdk-11.0.16+8` to `zulu-musl-11.74.15` (!211)

## v5.3.0
- upgrade `github.com/urfave/cli/v2` version [`v2.27.2` => [`v2.27.3`](https://github.com/urfave/cli/releases/tag/v2.27.3)] (!206)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3` version [`v3.1.0` => [`v3.1.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v3.1.1)] (!206)

## v5.2.0
- upgrade [`Spotbugs`](https://github.com/spotbugs/spotbugs) version [`4.8.3` => [`4.8.6`](https://github.com/spotbugs/spotbugs/releases/tag/4.8.6)] (!205)

## v5.1.0
- Update `command` package from `v2.2.0` to `v3.1.0` (!195)
- Update `common` package from `v3.2.3` to `v3.3.0` (!195)
- Update `report` package from `v4.3.2` to `v5.2.1` (!195)
- Update `ruleset` package from `v1.4.1` to `v3.1.0` (!195)
- Update version of `spotbugs` module from `v2` to `v5` (!195)

## v5.0.1
- update `go` version to `v1.22.3` (!202)

## v5.0.0
- Bump to next major version (!197)

## v4.3.9
- update `go` version to `v1.19` (!194)
- upgrade `github.com/gosimple/slug` version [`v1.13.1` => [`v1.14.0`](https://github.com/gosimple/slug/releases/tag/v1.14.0)] (!194)
- upgrade `github.com/stretchr/testify` version [`v1.8.4` => [`v1.9.0`](https://github.com/stretchr/testify/releases/tag/v1.9.0)] (!194)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.6` => [`v2.0.8`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.8)] (!194)

## v4.3.8
 - Always log SpotBugs process output on non-zero exit code. (!192)

## v4.3.7
 - Change the default Java max heap size from 1900M to 80%. (!193)

## v4.3.6
 - Refine the source code filter for better accuracy. (!191)

## v4.3.5
- upgrade [`Spotbugs`](https://github.com/spotbugs/spotbugs) version [`4.8.2` => [`4.8.3`](https://github.com/spotbugs/spotbugs/releases/tag/4.8.3)] (!186)
- upgrade `github.com/urfave/cli/v2` version [`v2.26.0` => [`v2.27.1`](https://github.com/urfave/cli/releases/tag/v2.27.1)] (!186)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.3.1` => [`v4.3.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.2)] (!186)

## v4.3.4
- upgrade [`Spotbugs`](https://github.com/spotbugs/spotbugs) version [`4.7.3` => [`4.8.2`](https://github.com/spotbugs/spotbugs/releases/tag/4.8.2)] (!185)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.7` => [`v2.26.0`](https://github.com/urfave/cli/releases/tag/v2.26.0)] (!185)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.2.0` => [`v4.3.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.3.1)] (!185)

## v4.3.3
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.2` => [`v3.2.3`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.3)] (!180)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.5` => [`v4.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.2.0)] (!180)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.4` => [`v2.0.6`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.6)] (!180)

## v4.3.2
- Switch to using official Maven `asdf` plugin (!181)

## v4.3.1
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.3` => [`v4.1.5`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.5)] (!175)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.3` => [`v2.0.4`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.4)] (!175)

## v4.3.0
- Add support for identifying the same vulnerability when moved within a file (!177)

## v4.2.2
- upgrade `github.com/urfave/cli/v2` version [`v2.25.5` => [`v2.25.7`](https://github.com/urfave/cli/releases/tag/v2.25.7)] (!174)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.2` => [`v4.1.3`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.3)] (!174)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2` version [`v2.0.2` => [`v2.0.3`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.3)] (!174)

## v4.2.1
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (!172)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (!172)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (!172)

## v4.2.0
- Allow `effort` scanner option to be configurable via `SAST_SCANNER_ALLOWED_CLI_OPTS` CI var (!171)
- Upgrade `security-products/analyzers/command` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (!171)

## v4.1.0
- Update `ruleset` module to `v2.0.2` to support loading remote Custom Rulesets (!170)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (!170)

## v4.0.0
- Bump to next major version (!167)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!167)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!167)

## v3.3.5
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!166)
- upgrade `github.com/urfave/cli/v2` version [`v2.24.3` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!166)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!166)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!166)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!166)

## v3.3.4
- Omit the `-quiet` flag when SECURE_LOG_LEVEL=debug (!164)

## v3.3.3
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!163)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.24.3`](https://github.com/urfave/cli/releases/tag/v2.24.3)] (!163)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!163)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.2` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!163)

## v3.3.2
- Omit the end line of a vulnerability's location if it's invalid (!159)

## v3.3.1
Update spotbugs to [`4.7.3`](https://github.com/spotbugs/spotbugs/releases/tag/4.7.3) (!155)

## v3.3.0
- upgrade [`Spotbugs`](https://github.com/spotbugs/spotbugs) version [`4.7.1` => [`4.7.2`](https://github.com/spotbugs/spotbugs/releases/tag/4.7.2)] (!154)
- upgrade `github.com/gosimple/slug` version [`v1.12.0` => [`v1.13.1`](https://github.com/gosimple/slug/releases/tag/v1.13.1)] (!154)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!154)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!154)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.0` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!154)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!154)

## v3.2.5
- Use 'assemble' task for gradle projects (!151 @sbrochet)

## v3.2.4
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!152)
- upgrade `github.com/stretchr/testify` version [`v1.7.1` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!152)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!152)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!152)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!152)

## v3.2.3
- Update spotbugs to [`4.7.1`](https://github.com/spotbugs/spotbugs/releases/tag/4.7.1) (!146)
- Update gradle to `7.5.0` (!146)
- Update grails to [`5.2.0`](https://github.com/grails/grails-core/releases/tag/v5.2.0) (!146)
- Update Java17 to `adoptopenjdk-17.0.3+8` (!146)
- Update Java11 to `adoptopenjdk-11.0.15+10` (!146)
- Update maven to `3.8.6` (!146)
- Update sbt to [`1.7.1`](https://github.com/sbt/sbt/releases/tag/v1.7.1) (!146)
- Update go dependencies (!146)

## v3.2.2
- Upgrade the `command` package for better analyzer messages (!150)

## v3.2.1
- Fix CA certs in OpenShift environments by granting `/etc/ssl/certs/ca-certificates.crt` root group perms (!148)

## v3.2.0
- Upgrade core analyzer dependencies (!147)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.1.0

- Use 'assemble' task for gradle projects (!130 @briceburg)
- Bump `spotbugs` version to [`4.7.0`](https://github.com/spotbugs/spotbugs/blob/4.7.0/CHANGELOG.md) (!140)
- Bump `find-sec-bugs` plugin to [`1.12.0`](https://github.com/find-sec-bugs/find-sec-bugs/releases/tag/version-1.12.0) (!140)
- Set Java 17 as system-wide default version (!142)
- Bump `gradle` version to `7.4.2` to support JDK 17 (!143)
- Bump `grails` version to `5.1.7` to support JDK 17 (!143)

## v3.0.0

- Remove JDK 8 support out of the box (!138)
- Bump build version of JDK 11(adoptopenjdk) to 101 (!138)
- Remove `JAVA_8_VERSION` and `JAVA_11_VERSION` CI variables in favor of `SAST_JAVA_VERSION` (!138)

## v2.30.3

- Update `command` to v1.7.0 (!136)

## v2.30.2

- Update spotbugs to [v4.6.0](https://github.com/spotbugs/spotbugs/blob/4.6.0/CHANGELOG.md) (!130)
- Update go dependencies (!130)

## v2.30.1

- Control Build tool logging based on `SECURE_LOG_LEVEL` variable (!129)

## v2.30.0

- Add java version 17 (!128)

## v2.29.2

- Log the error content/stack trace printed on the standard error stream instead of just exit status message (!127)
- Appropriate warning message on fallback Java version switch when the user does not set SAST_JAVA_VERSION variable or sets unsupported Java version (!127)

## v2.29.1

- Update ant to v1.10.12 (!123)
- Update go dependencies (!123)
  - Support ruleset overrides
- Update gradle to [v6.9.2](https://docs.gradle.org/6.9/release-notes.html) (!123)
- Update grails to [v4.0.13](https://github.com/grails/grails-core/releases/tag/v4.0.13) (!123)
- Update Java8 to adoptopenjdk-8.0.322+6 (!123)
- Update Java11 to adoptopenjdk-11.0.14+9 (!123)
- Update maven to 3.8.4 (!123)
- Update sbt to [v1.6.2](https://github.com/sbt/sbt/releases/tag/v1.6.2) (!123)
- Update scala to [v2.13.8](https://github.com/scala/scala/releases/tag/v2.13.8) (!123)

## v2.29.0

- Add env var to move asdf .tool-versions files out of the way for spotbugs to run (!122)

## v2.28.12

- Update spotbugs to [v4.5.3](https://github.com/spotbugs/spotbugs/blob/4.5.3/CHANGELOG.md) (!121)
- Bump sbt to [v1.6.1](https://github.com/sbt/sbt/releases/tag/v1.6.1) (!121)

## v2.28.11

- Fix custom CA certificate support (!119)
- Update sbt to v1.5.7 (!120)

## v2.28.10

- Update spotbugs to [v4.5.2](https://github.com/spotbugs/spotbugs/blob/4.5.2/CHANGELOG.md) (!117)

## v2.28.9

- Enable `log4j2.formatMsgNoLookups=true` by default in JVM configuration (!118)

## v2.28.8

- Update spotbugs to [v4.5.0](https://github.com/spotbugs/spotbugs/blob/4.5.0/CHANGELOG.md) (!115)

## v2.28.7

- Upgrade Go to v1.17 (!114)

## v2.28.6

- Update spotbugs to [v4.4.1](https://github.com/spotbugs/spotbugs/blob/4.4.1/CHANGELOG.md) (!112)
  - feat: Lower ExitCodes logger to debug level
- Update glibc to 2.34 (!112)
- Update gradle to [v6.9.1](https://docs.gradle.org/6.9/release-notes.html) (!112)
- Update grails to [v4.0.12](https://github.com/grails/grails-core/releases/tag/v4.0.12) (!112)
- Update maven to 3.8.2 (!112)
- Update Java8 to adoptopenjdk-8.0.302+8 (!112)
- Update Java11 to adoptopenjdk-11.0.12+7 (!112)

## v2.28.5

- Fix vulnerabilities found by gosec (!108)

## v2.28.4

- Update spotbugs to [v4.3.0](https://github.com/spotbugs/spotbugs/blob/4.3.0/CHANGELOG.md) (!106)
- Update ant to v1.10.11 (!102)
- Update gradle to v6.9.0 (!102)
- Update sbt to v1.5.5 (!102)

## v2.28.3

- Update gcc_libs to 10.2.0-6 (!101)
- Update asdf to v0.8.1 (!101)
- Update maven to 3.8.1 (!101)
- Update sbt to 1.5.3 (!101)
- Update scala to 2.13.6 (!101)

## v2.28.2

- Update ant to 1.10.10 (!97)
- Update glibc to 2.33-r0 (!97)
- Update Java8 to adoptopenjdk-8.0.292+10 (!97)
- Update Java11 to adoptopenjdk-11.0.11+9 (!97)

## v2.28.1

- Bumps spotbugs to [v4.2.3](https://github.com/spotbugs/spotbugs/blob/4.2.3/CHANGELOG.md) (!96)
  - Inconsistency in the description of `DLS_DEAD_LOCAL_INCREMENT_IN_RETURN`, `VO_VOLATILE_INCREMENT` and `QF_QUESTIONABLE_FOR_LOOP`
  - Should issue warning for SecureRandom object created and used only once
  - False positive `OBL_UNSATIFIED_OBLIGATION` with try with resources
  - `SA_LOCAL_SELF_COMPUTATION` bug
  - False positive `EQ_UNUSUAL` with record classes
- Update gradle to [v6.8.3](https://docs.gradle.org/6.8/release-notes.html) (!96)
- Update grails to [v4.0.10](https://github.com/grails/grails-core/releases/tag/v4.0.10) (!96)
- Update sbt to [v1.4.9](https://github.com/sbt/sbt/releases/tag/v1.4.9) (!96)
- Update scala to [v2.13.5](https://github.com/scala/scala/releases/tag/v2.13.5) (!96)

## v2.28.0

- Add support for Kotlin projects (!93)

## v2.27.0

- Update Dockerfile to support Open Shift (!95)

## v2.26.0

- Update report dependency in order to use the report schema version 14.0.0 (!94)

## v2.25.3

- Update spotbugs to v4.2.2 (https://github.com/spotbugs/spotbugs/blob/4.2.2/CHANGELOG.md) (!92)

## v2.15.2

- Update Java8 to adoptopenjdk-8.0.282+8 (!89)
- Update Java11 to adoptopenjdk-11.0.10+9 (!89)
- Update gradle to [6.8.1](https://docs.gradle.org/6.8/release-notes.html) (!89)
  - This version of Gradle removes TLS protocols v1.0 and v1.1 from the default list of allowed protocols
- Update grails to [4.0.6](https://github.com/grails/grails-core/releases/tag/v4.0.6) (!89)
- Update sbt to [1.4.6](https://github.com/sbt/sbt/releases/tag/v1.4.6) (!89)

## v2.15.1

- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!88)

## v2.15.0

- Update spotbugs to v4.2.0 (!85)
- Update sbt to v1.4.5 (!85)

## v2.14.1

- Update common to v2.22.0 (!78)
- Update urfave/cli to v2.3.0 (!78)

## v2.14.0

- Update spotbugs to v4.1.4, find-sec-bugs to v1.11.0 (!80)
- Update gcc to 10.2.0-4 (!80)
- Update ant to 1.10.9 (!80)
- Update Java8 to adoptopenjdk-8.0.275+1 (!80)
- Update Java11 to adoptopenjdk-11.0.9+101 (!80)
- Update gradle to 6.7.1 (!80)
- Update grails to 4.0.5 (!80)
- Update sbt to 1.4.3 (!80)
- Update scala to 2.13.4 (!80)
- Update golang dependencies logrus to 1.7.0, urfave/cli to 1.22.5 (!80)

## v2.13.6

- Update common to v2.21.3 (!79)

## v2.13.5

- Update common to v2.21.0 (!77)

## v2.13.4

- Fix unhandled error when setting ANT_HOME (!62)

## v2.13.3

- Match `adoptopenjdk-11.x` in asdf when using major_version `11` (!70 @widerin)

## v2.13.2

- Update `metadata.ScannerVersion` to match spotbugs version `4.1.2` (!68)

## v2.13.1

- Update golang dependencies (!67)

## v2.13.0

- Update glibc to v2.32-r0 (!64)
- Update gcc to v10.2.0-2 (!64)
- Update spotbugs to v4.1.2 (!64)
- Update gradle to v6.6.1 (!64)
- Update grails to v4.0.4 (!64)
- Update sbt to v1.3.13 (!64)
- Update scala to v2.13.3 (!64)
- Update golang dependencies to latest versions (!64)

## v2.12.0

- Switch from sdkman to asdf sdk manager (!56)

## v2.11.0

- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!59)

## v2.10.1

- Upgrade go to version 1.15 (!57)

## v2.10.0

- Add scan object to report (!52)

## v2.9.0

- Switch to the MIT Expat license (!48)

## v2.8.1

- Update sdkman checksum (!50)
- Update Java 8 version (!50)
- Update Java 11 version (!50)

## v2.8.0

- Update logging to be standardized across analyzers (!47)

## v2.7.1

- Add self-signed ceritficates for java projects (!43)

## v2.7.0

- Add `COMPILE` environment variable to skip project compilation (!42)

## v2.6.1

- Remove `location.dependency` from the generated SAST report (!40)

## v2.6.0

- Bump spotbugs to 4.0.2, glibc to 2.31-r0, zlib to 1.3.11-4, grails to 4.0.3, maven to 3.6.3, sbt to 1.3.10, scala to 2.13.1

## v2.5.1

- Bump SDKMAN to 5.8.1+484, Java 8 to 8.0.252, Java 11 to 11.0.7

## v2.5.0

- Add `id` field to vulnerabilities in JSON report (!33)

## v2.4.2

- Fix bug incorrectly attributing SpotBugs vulnerability to FindSecBugs (!30)

## v2.4.1

- update gradle to 5.6 (!31)

## v2.4.0

- Add support for custom CA certs (!28)

## v2.3.6

- Fixes setting Java 11 after sdkman breaking update (!26)

## v2.3.5

- update java 8 defined in analyze.go to release 8.0.242 (!25)
- update java 11 defined in analyze.go to release 11.0.6 (!25)

## v2.3.4

- update java 8 to release 8.0.242 (!22)
- update java 11 to release 11.0.6 (!22)

## v2.3.3

- update sdkman to latest v5.7.4+362 (!21)

## v2.3.2

- update java 8 to patch release 8.0.232 (!16 @haynes)
- update java 11 to patch release 11.0.5 (!16 @haynes)
- update findsecbugs to 1.10.1 (!16 @haynes)
- update sdkman to latest version 5.7.4+362 (!16 @haynes)

## v2.3.1

- added `--batch-mode` to the default `MAVEN_CLI_OPTS` to reduce the logsize

## v2.3.0

- Add an environment variable `SAST_JAVA_VERSION` to specify which Java version to use (8, 11)
  - Default version remains Java 8
  - Specific versions of Java 8/11 can be set using `JAVA_8_VERSION` and `JAVA_11_VERSION`

## v2.2.3

- Fix report JSON compare keys (`cve`) non-uniqueness by including file path and line information into them

## v2.2.2

- Switch primary `mvn`/`mvnw` build method from `compile` to `install`
- Support builds on cross-referential multi-module projects

## v2.2.1

- Update sdkman to latest version 5.7.3+337

## v2.2.0

- Change default behavior to exit early with non-zero exit code if compilation fails
- Introduce new `FAIL_NEVER` variable, to be set to `1` to ignore compilation failure

## v2.1.0

- Bump Spotbugs to [3.1.12](https://github.com/spotbugs/spotbugs/blob/3.1.12/CHANGELOG.md#3112---2019-02-28)
- Bump Find Sec Bugs to [1.9.0](https://github.com/find-sec-bugs/find-sec-bugs/releases/tag/version-1.9.0)

## v2.0.1

- Update common to v2.1.6

## v2.0.0

- Merge of the Maven, Gradle, Groovy and SBT analyzers with additional features:
  - Use the SpotBugs CLI for analysis.
  - Report correct source file paths.
  - Handle Maven multi module projects.
  - Only report vulnerabilities in source code files, ignore dependencies' libraries.
  - Add command line parameters and environment variable to set the paths of the Ant, Gradle, Maven, SBT and Java
    executables.
  - Add the `--mavenCliOpts` command line parameter and `MAVEN_CLI_OPTS` environment to pass arguments to Maven.

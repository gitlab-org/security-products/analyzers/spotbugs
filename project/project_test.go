package project

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"testing"

	log "github.com/sirupsen/logrus"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFindProjects(t *testing.T) {
	tests := []struct {
		name, excludedPaths, fileName string
		want                          int
	}{
		{
			name:          "Valid excluded paths",
			excludedPaths: "**/*.sbt,ant-project/**,gradle-project",
			want:          10,
		},
		{
			name:          "Empty excluded paths",
			excludedPaths: "",
			want:          12,
		},
	}

	// disable log output
	log.SetOutput(ioutil.Discard)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			projects, err := FindProjects(tt.excludedPaths, filepath.Join("..", "test", "fixtures"), true)
			require.NoError(t, err)
			require.Len(t, projects, tt.want)
		})
	}
}

func TestIsIgnored(t *testing.T) {
	tests := []struct {
		name, excludedPaths, fileName, wantLog string
		want                                   bool
	}{
		{
			name:          "Empty excluded paths",
			excludedPaths: "",
			fileName:      "some-project/some/file.txt",
			want:          false,
		},
		{
			name:          "Empty excluded paths with whitespace",
			excludedPaths: "    ",
			fileName:      "some-project/some/file.txt",
			want:          false,
		},
		{
			name:          "Valid excluded paths with non-matching glob pattern",
			excludedPaths: "tests/**",
			fileName:      "some-file.txt",
			want:          false,
		},
		{
			name:          "Valid excluded paths with glob pattern",
			excludedPaths: "some-project/**",
			fileName:      "some-project/some/file.txt",
			want:          true,
			wantLog:       `Skipping build file \"some-project/some-project/some/file.txt\" excluded by pattern: \"some-project/**\"`,
		},
		{
			name:          "Valid excluded paths with exact match",
			excludedPaths: "some-project/some/file.txt",
			fileName:      "some/file.txt",
			want:          true,
			wantLog:       `Skipping build file \"some-project/some/file.txt\" excluded by pattern: \"some-project/some/file.txt\`,
		},
	}

	repositoryPath := "/home/user/repo"
	buildfileDir := "/home/user/repo/some-project"

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logWriter := bytes.NewBuffer(nil)
			log.SetOutput(logWriter)

			got := isIgnored(tt.excludedPaths, repositoryPath, buildfileDir, tt.fileName)
			require.Equal(t, tt.want, got)

			if tt.wantLog != "" {
				logOutput := logWriter.String()
				require.Contains(t, logOutput, tt.wantLog)
			}
		})
	}
}

func TestNewProject(t *testing.T) {
	emptyPath := filepath.Join("..", "test", "empty")
	projectPath := filepath.Join("..", "test", "fixtures", "maven-project")
	type args struct {
		path string
	}
	tests := []struct {
		name        string
		args        args
		wantBuilder string
		wantErr     bool
	}{
		{
			name: "Empty",
			args: args{
				path: emptyPath,
			},
			wantBuilder: "",
			wantErr:     true,
		},
		{
			name: "Project",
			args: args{
				path: projectPath,
			},
			wantBuilder: "Maven",
			wantErr:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, err := newProject(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("newProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr {
				if _, ok := err.(errNoCompatibleBuilder); !ok {
					t.Errorf(
						"newProject() error type = %s, wanted error type errNoCompatibleBuilder",
						reflect.TypeOf(err))
					return
				}
			} else {
				if p.builder.name != tt.wantBuilder {
					t.Errorf(
						"newProject() detected builder is wrong wanted %s got %s",
						tt.wantBuilder,
						p.builder.name)
					return
				}
			}
		})
	}
}

func TestProject_Packages(t *testing.T) {
	tests := []struct {
		name        string
		projectPath string
		want        []string
	}{
		{
			name:        "Groovy",
			projectPath: filepath.Join("..", "test", "fixtures", "groovy-project"),
			want:        []string{"com.gitlab.security_products.tests"},
		},
		{
			name:        "Java",
			projectPath: filepath.Join("..", "test", "fixtures", "maven-project"),
			want:        []string{"com.gitlab.security_products.tests"},
		},
		{
			name:        "Scala",
			projectPath: filepath.Join("..", "test", "fixtures", "sbt-project"),
			want:        []string{"com.example"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, err := newProject(tt.projectPath)
			if err != nil {
				t.Errorf("%s\n", err.Error())
				return
			}

			if got := p.Packages(); !reflect.DeepEqual(got, tt.want) {
				absPath, err := filepath.Abs(tt.projectPath)
				if err != nil {
					cur, _ := os.Getwd()
					t.Errorf("Project.Packages() no absolute path exist for %s (current dir: %s )", tt.projectPath, cur)
				}
				t.Errorf("Project.Packages() for %s = %v, want %v", absPath, got, tt.want)
			}
		})
	}
}

func TestProject_GetSourcePathRelativeToProject(t *testing.T) {
	tests := []struct {
		name         string
		projectPath  string
		reportedPath string
		wantResult   string
		wantErr      bool
	}{
		{
			name:         "Groovy",
			projectPath:  filepath.Join("..", "test", "fixtures", "groovy-project"),
			reportedPath: "com/gitlab/security_products/tests/App.groovy",
			wantResult:   "src/main/groovy/com/gitlab/security_products/tests/App.groovy",
			wantErr:      false,
		},
		{
			name:         "Java",
			projectPath:  filepath.Join("..", "test", "fixtures", "maven-project"),
			reportedPath: "com/gitlab/security_products/tests/App.java",
			wantResult:   "src/main/java/com/gitlab/security_products/tests/App.java",
			wantErr:      false,
		},
		{
			name:         "Scala",
			projectPath:  filepath.Join("..", "test", "fixtures", "sbt-project"),
			reportedPath: "com/example/Main.scala",
			wantResult:   "src/main/scala/com/example/Main.scala",
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, err := newProject(tt.projectPath)
			if err != nil {
				t.Errorf("%s\n", err.Error())
				return
			}

			absPath, err := filepath.Abs(tt.projectPath)
			if err != nil {
				cur, _ := os.Getwd()
				t.Errorf("Project.Packages() no absolute path exist for %s (current dir: %s )", tt.projectPath, cur)
			}

			gotResult, err := p.RelativePath(tt.reportedPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("Project.RelativePath() for %s error = %v, wantErr %v", absPath, err, tt.wantErr)
				return
			}
			if gotResult != tt.wantResult {
				t.Errorf("Project.RelativePath() for %s = %v, want %v", absPath, gotResult, tt.wantResult)
			}
		})
	}
}

func TestProject_recordSourceFiles(t *testing.T) {
	// Create a temporary project and add files.
	projectFiles := []struct {
		// skip is set to true if, based on path alone, the file should be
		// skipped, i.e. not considered project source code for bug reporting.
		skip bool
		// path relative to project root.
		relPath string
	}{
		{true, "build.gradle"},
		{true, "gradle/foo1.kt"},
		{true, "gradle/wrapper/gradle-wrapper.jar"},
		{true, "gradle/wrapper/gradle-wrapper.properties"},
		{true, ".git/HEAD"},
		{true, ".git/config"},
		{true, ".gradle/7.5.1/executionHistory"},
		{true, ".gradle/foo2.kt"},
		{false, "src/foo3.kt"},
		{false, "src/gradle/foo4.kt"},
		{false, "src/gradle/bar/foo5.kt"},
		{false, "src/.git/bar/foo5.kt"},
		{false, "src/.gradle/bar/foo5.kt"},
	}

	tempDir, err := os.MkdirTemp("/tmp", "test-")
	require.NoError(t, err)
	defer os.RemoveAll(tempDir)

	for _, f := range projectFiles {
		err = touch(filepath.Join(tempDir, f.relPath))
		if err != nil {
			t.Fatal(err)
		}
	}

	// Create project object.  In newProject, Project.recordSourceFiles walks
	// the project and collects info used later by Project.RelativePath to
	// determine if a file is within the project or not.
	proj, err := newProject(tempDir)
	require.NoError(t, err)

	for _, f := range projectFiles {
		_, err := proj.RelativePath(f.relPath)
		assert.Equal(t, f.skip, err != nil, f.relPath)
	}
}

// touch creates a file and along with any necessary parent directories and
// return nil or return an error.
func touch(path string) error {
	dir := filepath.Dir(path)
	err := os.MkdirAll(dir, 0777)
	if err != nil {
		return err
	}

	f, err := os.Create(path)
	if err != nil {
		return err
	}
	f.Close()
	return nil
}

package project

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v5/utils"
)

func Test_withCleanup(t *testing.T) {
	// Create test directory
	tempDir, err := os.MkdirTemp("/tmp", "test-")
	require.NoError(t, err)

	tempFile, err := os.CreateTemp(tempDir, "test-")
	require.NoError(t, err)

	// Create a file in a function wrapped by withCleanup
	_ = withCleanup(tempDir, func() error {
		_, err = os.CreateTemp(tempDir, "created-")
		if err != nil {
			return err
		}

		return utils.NewRunCmdError(1, "fail")
	})

	// Check that only the test- file remains in the tempDir after execution
	files, err := os.ReadDir(tempDir)

	if len(files) != 1 || files[0].Name() != filepath.Base(tempFile.Name()) {
		// Failed to cleanup
		t.Error("withCleanup() didn't clean up created file.")
		return
	}
}

func Test_withFileRestoration(t *testing.T) {
	t.Run("Failure during build", func(t *testing.T) {
		tempFile, err := os.CreateTemp("", "test-*")
		require.NoError(t, err)

		build := func() error { return errors.New("build error") }

		procedure := withFileRestoration(tempFile.Name(), build)

		err = procedure()
		require.EqualError(t, err, "build error")

		backupFile := tempFile.Name() + ".bak"
		_, err = os.Stat(backupFile)

		// the backup file should be removed, even if the build() function fails
		require.True(t, os.IsNotExist(err),
			fmt.Sprintf("Expected file %q to not exist", backupFile))
	})
}

func Test_deleteEmpty(t *testing.T) {
	want := []string{"aa", "bb", "cc"}
	got := deleteEmpty([]string{"aa", "", "bb", "cc"})
	require.Equal(t, want, got)
}

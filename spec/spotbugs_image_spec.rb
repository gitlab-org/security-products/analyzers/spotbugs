require 'tmpdir'
require 'English'

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

RSpec.shared_examples 'logs compile warning' do
  it 'logs warning about compiling from source' do
    expect(scan.combined_output).to match(/WARN.*The analyzer is set to compile from source\. Troubleshooting is not covered by support/)
  end
end

RSpec.shared_examples 'successful compile' do
  include_examples 'logs compile warning'

  it 'does not log build failure error' do
    expect(scan.combined_output).not_to match(/ERRO.*Building from source failed\. You should pre-compile your code and try again/)
  end
end

RSpec.shared_examples 'failed compile' do
  include_examples 'logs compile warning'

  it 'logs build failure error' do
    expect(scan.combined_output).to match(/ERRO.*Building from source failed\. You should pre-compile your code and try again/)
  end
end

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def image_name
    ENV.fetch('TMP_IMAGE', 'spotbugs:latest')
  end

  def target_mount_dir
    '/app'
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --user=gitlab --rm -w #{target_mount_dir} #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in #{target_mount_dir}/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = "gl-sast-report.json")
      path = File.join(expectations_dir, expectation_name, report_name)
      if ENV['REFRESH_EXPECTED'] == "true"
        # overwrite the expected JSON with the newly generated JSON
        FileUtils.cp(scan.report_path, File.expand_path(path))
      end
      JSON.parse(File.read(path))
    end

    let(:secure_log_level) { 'debug' }
    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR is needed for `post-analyzers/scripts` to
        # properly resolve file locations
        # https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': target_mount_dir,
        'SEARCH_MAX_DEPTH': 20,
        'SECURE_LOG_LEVEL': secure_log_level,
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @output_dir,
        command: command,
        script: script,
        offline: offline,
        user: 'gitlab',
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json'
      )
    end

    let(:report) { scan.report }

    context 'when using grails with gradle build tool' do
      context 'when the project cannot be built' do
        let(:project) { 'grailsw/build-fails' }

        it_behaves_like 'failed compile'
        it_behaves_like 'failed scan'

        it 'exits with status code 1' do
          expect(scan.combined_output).to match(/ERRO.*Project couldn't be built: exit status 1/)
        end

        it 'does not display a grails-specific log message' do
          expect(scan.combined_output).not_to match(/grails failed to compile the project/)
        end

        context 'and compilation failures are ignored because FAIL_NEVER has been set to true' do
          let(:variables) { { 'FAIL_NEVER': true } }

          it 'shows a compilation failure message' do
            expect(scan.combined_output).to match(/Building from source failed.*Attempting scan anyway/)
          end
        end
      end

      context 'when the project can be built, but outputs BUILD FAILED' do
        let(:project) { 'grailsw/build-succeeds-with-failure-message' }

        it_behaves_like 'failed compile'
        it_behaves_like 'failed scan'

        it 'exits with status code 1 and displays a grails-specific error message' do
          expect(scan.combined_output).to match(/ERRO.*Project couldn't be built: grails failed to compile the project/)
        end
      end
    end

    context 'when using kotlin with gradle build tool' do
      let(:project) { 'kotlin' }

      it_behaves_like 'successful compile'

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'when using groovy with gradle build tool' do
      context 'default' do
        let(:project) { 'groovy/default' }

        it_behaves_like 'successful compile'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report(project)
            }
          end
          it_behaves_like 'valid report'
        end

        context 'when testing log output' do
          context 'when SECURE_LOG_LEVEL is set to debug' do

            it 'streams the output of the spotbug command at the info level' do
              expect(scan.combined_output).to match(/\[INFO\] \[Spotbugs\] \[.*\] ▶ SpotBugs analysis succeeded.*/)
            end

            it 'streams additional output because the spotbugs quiet flag has not been enabled' do
              expect(scan.combined_output).to match(/\[INFO\] \[Spotbugs\] \[.*\] ▶ BUILD SUCCESSFUL.*/)
            end
          end

          context 'when SECURE_LOG_LEVEL is not set' do
            let(:secure_log_level) { nil }

            it 'streams the output of the spotbug command at the info level' do
              expect(scan.combined_output).to match(/\[INFO\] \[Spotbugs\] \[.*\] ▶ SpotBugs analysis succeeded.*/)
            end

            it 'does not stream additional output because the spotbugs quiet flag has been enabled' do
              expect(scan.combined_output).not_to match(/\[INFO\] \[Spotbugs\] \[.*\] ▶ BUILD SUCCESSFUL.*/)
            end
          end
        end
      end

      context 'with disabled rules' do
        let(:project) { 'groovy/with-disabled-rules' }

        it_behaves_like 'successful compile'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report(project)
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'with no classfiles' do
        let(:project) { 'groovy/no-classfiles' }

        it_behaves_like 'successful compile'
        it_behaves_like "successful scan"

        it 'outputs a custom warning message when the pom.xml does not have any classfiles' do
          expect(scan.combined_output).to match(/WARN.*SpotBugs didn't find any class file to analyze in/)
        end
      end
    end

    context 'when using mono-repository containing scala, kotlin and groovy language files' do
      let(:project) { 'monorepo/default' }

      it_behaves_like 'successful compile'

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end

      context 'when excluding paths' do
        context 'with SAST_EXCLUDED_PATHS' do
          context 'when the excluded scala path exactly matches the build file' do
            let(:variables) do
              {
                'SAST_EXCLUDED_PATHS': 'groovy/**,scala/*.sbt'
              }
            end

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) {
                  parse_expected_report('monorepo/excluding-groovy')
                }
              end
              it_behaves_like 'valid report'
            end
          end

          # test for https://gitlab.com/gitlab-org/gitlab/-/issues/484707#note_2133468684
          context 'when the excluded groovy path does not match anything' do
            let(:variables) do
              {
                'SAST_EXCLUDED_PATHS': 'groovy/**/app'
              }
            end

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) {
                  parse_expected_report(project)
                }
              end
              it_behaves_like 'valid report'
            end
          end
        end

        context 'with SAST_SPOTBUGS_EXCLUDED_BUILD_PATHS' do
          context 'when excluding groovy build files' do
            let(:variables) do
              {
                'SAST_SPOTBUGS_EXCLUDED_BUILD_PATHS': 'groovy/app/*'
              }
            end

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) {
                  parse_expected_report('monorepo/excluding-groovy')
                }
              end
              it_behaves_like 'valid report'

              it 'skips excluded build files' do
                expect(scan.combined_output)
                  .to match(%r{Skipping build file "groovy/app/gradlew" excluded by pattern: "groovy/app/\*"})
              end
            end
          end

          context 'when excluding groovy and scala build files' do
            let(:variables) do
              {
                'SAST_SPOTBUGS_EXCLUDED_BUILD_PATHS': 'groovy/**,scala/**'
              }
            end

            describe 'created report' do
              it_behaves_like 'non-empty report'
              it_behaves_like 'recorded report' do
                let(:recorded_report) {
                  parse_expected_report('monorepo/excluding-groovy-and-scala')
                }
              end
              it_behaves_like 'valid report'

              it 'skips excluded build files' do
                expect(scan.combined_output)
                  .to match(%r{Skipping build file "groovy/app/gradlew" excluded by pattern: "groovy/\*\*"})

                expect(scan.combined_output)
                  .to match(%r{Skipping build file "scala/build.sbt" excluded by pattern: "scala/\*\*"})
              end
            end

            context 'and the excluded scala path exactly matches the build file' do
              let(:variables) do
                {
                  'SAST_SPOTBUGS_EXCLUDED_BUILD_PATHS': 'groovy/**,scala/*.sbt'
                }
              end

              describe 'created report' do
                it_behaves_like 'non-empty report'
                it_behaves_like 'recorded report' do
                  let(:recorded_report) {
                    parse_expected_report('monorepo/excluding-groovy-and-scala')
                  }
                end
                it_behaves_like 'valid report'

                it 'skips excluded build files' do
                  expect(scan.combined_output)
                    .to match(%r{Skipping build file "groovy/app/gradlew" excluded by pattern: "groovy/\*\*"})

                  expect(scan.combined_output)
                    .to match(%r{Skipping build file "scala/build.sbt" excluded by pattern: "scala/\*.sbt"})
                end
              end
            end
          end
        end
      end
    end

    context 'when using scala with SBT build tool' do
      let(:project) { 'scala' }

      it_behaves_like 'successful compile'

      describe 'created report' do
        it_behaves_like 'non-empty report'
        it_behaves_like 'recorded report' do
          let(:recorded_report) {
            parse_expected_report(project)
          }
        end
        it_behaves_like 'valid report'
      end
    end

    context 'when using maven' do
      context 'by default' do
        let(:project) { 'maven/default' }

        it_behaves_like 'successful compile'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report(project)
            }
          end
          it_behaves_like 'valid report'

          context 'when the version of Java is not specified' do
            it 'falls back to the default version of Java' do
              expect(scan.combined_output).to match(
                /Java version not set in SAST_JAVA_VERSION variable\. Using Java version "21" as a fallback\./
              )
            end
          end

          context 'when Java version 21 is configured' do
            let(:variables) do
              { 'SAST_JAVA_VERSION': '21' }
            end

            it 'uses the configured version of Java' do
              expect(scan.combined_output).to match(/SAST_JAVA_VERSION was configured, using Java version "21"/)
            end
          end
        end
      end

      context 'Java 17' do
        let(:project) { 'maven/java17' }
        let(:variables) do
          { 'SAST_JAVA_VERSION': '17' }
        end

        it_behaves_like 'successful compile'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('maven/default')
            }
          end

          it_behaves_like 'valid report'

          it 'uses the configured version of Java' do
            expect(scan.combined_output).to match(/SAST_JAVA_VERSION was configured, using Java version "17"/)
          end
        end
      end

      context 'bugfix for issue 492092: Spotbugs fails when used with projects requiring libstdc++' do
        arch = `docker run #{ENV['TMP_IMAGE']} arch`.chomp
        skip_reason = 'Skipping example because arm64 architecture was detected. This example can only be executed ' \
                      'on amd64 (x86_64) architecture. ' \
                      'Please see https://gitlab.com/gitlab-org/gitlab/-/issues/492269 for more details.'

        describe 'created report', skip: arch == 'aarch64' ? skip_reason : nil do
          let(:project) { 'maven/492092-project-requiring-libstdc-plus-plus' }

          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('maven/default')
            }
          end
          it_behaves_like 'valid report'
        end
      end

      context 'when including tracking signatures' do
        let(:project) { 'maven/default' }
        let(:variables) do
          {
            'GITLAB_FEATURES': 'vulnerability_finding_signatures'
          }
        end

        it_behaves_like 'successful compile'

        describe 'created report' do
          it_behaves_like 'non-empty report'
          it_behaves_like 'recorded report' do
            let(:recorded_report) {
              parse_expected_report('maven/with-tracking')
            }
          end
          it_behaves_like 'valid report'
        end
      end
    end
  end
end

package scanneropts

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v3"
)

type scannerOptsMap map[string]cliOpt

func (s scannerOptsMap) String() string {
	names := make([]string, 0, len(s))
	for name := range s {
		names = append(names, fmt.Sprintf("%q", name))
	}
	return strings.Join(names, ", ")
}

// allowedScannerOpts is the set of CLI options that are allowed to pass to
// the underlying security scanner.
// see https://gitlab.com/gitlab-org/gitlab/-/issues/368565
var allowedScannerOpts = scannerOptsMap{
	// effort: https://spotbugs.readthedocs.io/en/stable/effort.html
	"-effort": {
		name:       "-effort",
		defaultVal: "max",
		mandatory:  true,
		separator:  ":",
	},
}

type cliOpt struct {
	name       string
	defaultVal string // fallback if mandatory + user not provided the input
	userVal    string
	mandatory  bool   // to include this CLI option with default value if user doesn't include this CLI option
	separator  string // can be a colon(:) or space(' ')
}

func (o cliOpt) build() []string {
	targetValue := o.userVal
	if targetValue == "" && o.mandatory {
		targetValue = o.defaultVal
	}

	if targetValue == "" {
		return nil // no-op
	}

	return strings.Split(o.name+o.separator+targetValue, " ")
}

// ParseConfigurableOpts gathers all user configurable CLI options
// that are passed to the Scanner.
// 1. Parse the `SAST_SCANNER_ALLOWED_CLI_OPTS` input
// to filter only CLI options that are defined in `allowedScannerOpts`.
// 2. Include all the missing CLI options that are mandatory but the
// user did not include in the CI var input
func ParseConfigurableOpts(ciVarOptsStr string) []string {
	ciVarOptsStr = strings.TrimSpace(ciVarOptsStr)
	if ciVarOptsStr == "" {
		return nil
	}
	userArgs, invalid := command.ParseCIVar(ciVarOptsStr)
	if len(invalid) > 0 {
		log.Warn("Skipping following arguments as they were provided in the wrong format. Refer to https://docs.gitlab.com/ee/user/application_security/sast/#analyzer-settings to learn more")
		log.Warnf("%s\n", strings.Join(invalid, ", "))
	}

	// Create a local copy of the configurableScannerOpts map,
	// with the values stored with pointer semantics, so
	// we can add in the user-supplied arg value as necessary.
	opts := make(map[string]*cliOpt)
	for k, v := range allowedScannerOpts {
		opts[k] = &v
	}

	for _, arg := range userArgs {
		opt, ok := opts[arg.Name]
		if !ok {
			log.Warnf("%q will be ignored. Allowed values are: %s", arg.Name, allowedScannerOpts)
			continue
		}
		opt.userVal = arg.Value
	}

	gatheredArgs := make([]string, 0, len(userArgs)+len(opts))
	for _, opt := range opts {
		gatheredArgs = append(gatheredArgs, opt.build()...)
	}

	return gatheredArgs

}
